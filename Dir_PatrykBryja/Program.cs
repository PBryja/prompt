﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dir_PatrykBryja
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Wpisz cmd, aby poznac wszystkie komendy\n");
            string currentDir;
            string command = String.Empty;
            string command2 = String.Empty;
            int index = -1;
            bool exit = false;
            CommandManager manager = new CommandManager();

            if (args.Length > 0) //mozna wypisac parametry przy uruchamianiu programu, np. cmd> Dir_PatrykBryja color modified
            {
                string str = String.Join(" ", args);
                manager.ExecuteManager("set", str);
            }

            while (!exit)
            {
                index = -1;
                command2 = String.Empty;
                currentDir = Directory.GetCurrentDirectory();
                Console.Write(currentDir + ">");

                try
                {
                    command = Console.ReadLine().Trim();

                }
                catch (Exception e)
                {
                    exit = true;
                }

                index = command.IndexOf(' ');

                if (index > 0)
                {
                    command2 = command.Remove(0, index + 1);
                    command = command.Remove(index);
                    manager.ExecuteManager(command, command2);
                }
                else if (command == "exit")
                    exit = true;
                else if (command.Length > 0)
                    manager.ExecuteManager(command);
            }
        }
    }
}
