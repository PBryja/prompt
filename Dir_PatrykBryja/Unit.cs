﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dir_PatrykBryja
{
    class Unit
    {
        protected string name;
        protected string extension;
        protected string type;
        protected string path;
        protected DateTime modified;
        protected DateTime accessed;
        protected DateTime created;
        protected long size;

        public long Size { get => size; set => size = value; }
        public string Name { get => name; set => name = value; }
        public string Extension { get => extension; set => extension = value; }
        public string Type { get => type; set => type = value; }
        public DateTime Modified { get => modified; set => modified = value; }
        public DateTime Accessed { get => accessed; set => accessed = value; }
        public DateTime Created { get => created; set => created = value; }
        public string Path { get => path; set => path = value; }
    }
}
