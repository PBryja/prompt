﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dir_PatrykBryja
{
    class CommandManager
    {
        private Dictionary<string, bool> parameters = new Dictionary<string, bool>
        {
            {"color", false },
            { "sortAlphInc", false },
            { "sortAlphDec", false },
            { "size", false },
            { "created", false },
            { "accessed", false },
            { "modified", false },
            { "sortSizeInc", false },
            { "sortSizeDec", false },
            { "sortModifiedInc", false },
            { "sortModifiedDec", false },
            { "sortCreatedInc", false },
            { "sortCreatedDec", false },
            { "sortAccessedInc", false },
            { "sortAccessedDec", false },
        };

        public void ShowParameters()
        {
            string str = "";
            foreach (var item in parameters)
            {
                str += item.Key + " - " + item.Value.ToString() + "\n";
            }
            Console.WriteLine(str);
        }

        public void SetParameters(string cmd)
        {
            string[] array;
            array = cmd.Split(' ');

            foreach (var item in array)
                if (parameters.ContainsKey(item))
                {
                    parameters[item] = !parameters[item];
                }
        }

        public void ShowCommands()
        {
            string str = "Komendy: \n";
            str += "dir lub ls - wyswietla liste katalogow oraz plikow w biezacym katalogu\n";
            str += "cd .. lub cd nazwa - przejscie do katalogu\n";
            str += "exit - wyjscie z programu\n";
            str += "parameters - wyswietla liste parametrow oraz ich stan\n";
            str += "set NAZWA - zmienia status danego parametru na true lub false (negacja)\n" +
                "przyklad: set color : ustawia parametr color na true, od teraz beda kolorowane informacje dot. plikow podczas ich wyswietlania (dir)\n" +
                "przyklad2: set color created : teraz color jest ustawiony na false, ale created jest ustawiony na true, czyli bedzie wyswietlana informacja kiedy powstal dany plik\n" +
                "mozna po slowie set wypisac kilka parametrow, np. set color modified created\n\n";
            str += "Lista wszystkich parametrow:\n";

            foreach (var item in parameters)
            {
                str+=item.Key+"\n";
            }

            Console.WriteLine(str);

        }

        public void ExecuteManager(string commandMain, string commandAdditional = "")
        {
            switch (commandMain)
            {

                case "dir":
                    ShowList(PrepareList());
                    break;
                case "ls":
                    ShowList(PrepareList());
                    break;
                case "cd":
                    OpenNewFolder(commandAdditional);
                    break;
                case "cd..":
                    OpenNewFolder("..");
                    break;
                case "set":
                    SetParameters(commandAdditional);
                    break;
                case "cmd":
                    ShowCommands();
                    break;
                case "parameters":
                    ShowParameters();
                    break;
                default:
                    break;
            }

        }

        private void OpenNewFolder(string folder)
        {
            if (Directory.Exists(folder))
            {
                Directory.SetCurrentDirectory(folder + "\\");
            }
            else
                Console.WriteLine("System nie może odnaleźć określonej ścieżki.");

        }

        private void SetColor(string extension)
        {
            switch (extension)
            {

                case ".exe":
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        break;
                    }
                case ".cs":
                    {
                        Console.ForegroundColor = ConsoleColor.DarkMagenta;
                        break;
                    }
                case ".txt":
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    }
                case "":
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        break;
                    }
                case ".pdf":
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        break;
                    }
                case ".jpg":
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        break;
                    }
                default:
                    {
                        Console.ForegroundColor = ConsoleColor.Gray;
                        break;
                    }
            }
        }

        private Unit AddToUnit(Unit unit, string item)
        {
            DirectoryInfo info = new DirectoryInfo(item);
            unit.Path = Path.GetFullPath(item);
            unit.Name = Path.GetFileName(item);
            unit.Modified = Directory.GetLastWriteTime(item);
            unit.Accessed = Directory.GetLastAccessTime(item);
            unit.Created = Directory.GetCreationTime(item);
            return unit;
        }

        public List<Unit> PrepareList()
        {
            List<Unit> listUnits = new List<Unit>();

            string[] files = Directory.GetFileSystemEntries(@".");

            foreach (var item in files)
            {

                if (File.Exists(item))
                {
                    FileInfo info = new FileInfo(item);
                    FileUnit unit = new FileUnit();
                    unit.Size = new FileInfo(item).Length;
                    unit.Type = "file";
                    unit.Extension = info.Extension;
                    listUnits.Add((FileUnit)AddToUnit(unit, item));
                }
                else if (Directory.Exists(item))
                {
                    DirectoryUnit unit = new DirectoryUnit();
                    unit.Size = -1;
                    unit.Type = "directory";
                    unit.Extension = "";
                    listUnits.Add((DirectoryUnit)AddToUnit(unit, item));
                }
            }


            if (parameters["sortAlphDec"])
                listUnits = listUnits.OrderByDescending(o => o.Name).ToList();
            else if (parameters["sortAlphInc"])
                listUnits = listUnits.OrderBy(o => o.Name).ToList();

            if (parameters["sortSizeDec"])
                listUnits = listUnits.OrderByDescending(o => o.Size).ToList();
            else if (parameters["sortSizeInc"])
                listUnits = listUnits.OrderBy(o => o.Size).ToList();

            if (parameters["sortModifiedDec"])
                listUnits = listUnits.OrderByDescending(o => o.Modified).ToList();
            else if (parameters["sortModifiedInc"])
                listUnits = listUnits.OrderBy(o => o.Modified).ToList();

            if (parameters["sortCreatedDec"])
                listUnits = listUnits.OrderByDescending(o => o.Created).ToList();
            else if (parameters["sortCreatedInc"])
                listUnits = listUnits.OrderBy(o => o.Created).ToList();

            if (parameters["sortAccessedDec"])
                listUnits = listUnits.OrderByDescending(o => o.Accessed).ToList();
            else if (parameters["sortAccessedInc"])
                listUnits = listUnits.OrderBy(o => o.Accessed).ToList();


            return listUnits;
        }

        public void ShowList(List<Unit> files)
        {
            string str = "";

            foreach (Unit item in files)
            {
                if (parameters["size"])
                {
                    if (item.Size != -1)
                        str = "size:     " + item.Size + " b\n" + str;
                }
                if (parameters["modified"])
                    str = "modified: " + item.Modified + "\n" + str;
                if (parameters["created"])
                    str = "created:  " + item.Created + "\n" + str;
                if (parameters["accessed"])
                    str = "accessed: " + item.Accessed + "\n" + str;
                if (str.Length > 0)
                    str += " | " + item.Name + "\n";
                else
                    str = item.Name;

                if (parameters["color"])
                    SetColor(item.Extension);

                Console.WriteLine(str);
                Console.ForegroundColor = ConsoleColor.Gray;
                str = "";
            }
        }

    }
}
