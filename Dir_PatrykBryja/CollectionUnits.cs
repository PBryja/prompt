﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dir_PatrykBryja
{
    class CollectionUnits : Unit
    {
        private List<FileUnit> fileUnits;
        private List<DirectoryUnit> directoryUnits;

        public List<FileUnit> FileUnits { get => fileUnits; set => fileUnits = value; }
        public List<DirectoryUnit> DirectoryUnits { get => directoryUnits; set => directoryUnits = value; }
    }
}
